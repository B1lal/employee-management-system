# Employee Management System

Simple CRUD operation frontend application which allows a user to:

- Create new employees using csv file upload.
- List all the employees in a table with sorting/ordering
- Editing the employees
- Deleting the employees

## Installation

Install employee-managment-service with yarn

```bash
  cd employee-managment-service //navigate to the directory
  yarn install                  //install the dependencies
  yarn start                    //run the server
```

## Tech Stack

**Client:** React, AntDesign

**Server:** https://beeceptor.com/console/nphc-hr

## API Reference

#### Get all employees

```http
  GET /employees
```

#### Updates an employees

```http
  PATCH /employees/:id
```

| Parameter | Type     | Description                       |
| :-------- | :------- | :-------------------------------- |
| `id`      | `string` | **Required**. Id of item to patch |

#### Deletes an employees

```http
  DELETE /employees/:id
```

| Parameter | Type     | Description                        |
| :-------- | :------- | :--------------------------------- |
| `id`      | `string` | **Required**. Id of item to delete |

## Appendix

In case the endpoint https://beeceptor.com/console/nphc-hr reaches its daily limit, the app loads dummy data from the component to list the table.
