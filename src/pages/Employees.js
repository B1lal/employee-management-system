/*!
=========================================================
* Muse Ant Design Dashboard - v1.0.0
=========================================================
* Product Page: https://www.creative-tim.com/product/muse-ant-design-dashboard
* Copyright 2021 Creative Tim (https://www.creative-tim.com)
* Licensed under MIT (https://github.com/creativetimofficial/muse-ant-design-dashboard/blob/main/LICENSE.md)
* Coded by Creative Tim
=========================================================
* The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
*/
import {
  Row,
  Col,
  Card,
  Table,
  Upload,
  message,
  Space,
  Form,
  Input,
  Button,
  Avatar,
  Typography,
  Popconfirm,
  Modal,
  InputNumber,
} from "antd";
import { QuestionCircleOutlined } from "@ant-design/icons";

import React, { useState, useEffect } from "react";
import axios from "axios";

import { ToTopOutlined } from "@ant-design/icons";
import { BrowserRouter, Link } from "react-router-dom";

const { Title } = Typography;
// DUMMY DATA TO SEE THE DATA IF THE ENDPOINT DOESN'T WORK DUE TO PRICING ISSUE,
// THE ENDPOINT IN GIVE IN THE ASSIGNMENT DON"T EVEN WORK, SO I HAVE TO USE DUMMY DATA,
// EXPECTED THE ENDPOINT TO BE WORKING FOR THE FRONTEND ASSIGNMENT !!
const DATA = [
  {
    id: 1,
    full_name: "Corrianne Graffham",
    login_id: "cgraffham0",
    salary: 8214.74,
    profile_pic: "http://dummyimage.com/156x100.png/cc0000/ffffff",
  },
  {
    id: 2,
    full_name: "Randolf Kaesmakers",
    login_id: "rkaesmakers1",
    salary: 9189.6,
    profile_pic: "http://dummyimage.com/108x100.png/ff4444/ffffff",
  },
  {
    id: 3,
    full_name: "Nobe Todarini",
    login_id: "ntodarini2",
    salary: 9217.6,
    profile_pic: "http://dummyimage.com/216x100.png/5fa2dd/ffffff",
  },
  {
    id: 4,
    full_name: "Myca Gromley",
    login_id: "mgromley3",
    salary: 8379.2,
    profile_pic: "http://dummyimage.com/222x100.png/dddddd/000000",
  },
  {
    id: 5,
    full_name: "Gusella Francescotti",
    login_id: "gfrancescotti4",
    salary: 9033.58,
    profile_pic: "http://dummyimage.com/125x100.png/dddddd/000000",
  },
  {
    id: 6,
    full_name: "Reese Potter",
    login_id: "rpotter5",
    salary: 9135.62,
    profile_pic: "http://dummyimage.com/116x100.png/ff4444/ffffff",
  },
  {
    id: 7,
    full_name: "Zachary Meakes",
    login_id: "zmeakes6",
    salary: 8226.0,
    profile_pic: "http://dummyimage.com/216x100.png/5fa2dd/ffffff",
  },
  {
    id: 8,
    full_name: "Cozmo O'Spillane",
    login_id: "cospillane7",
    salary: 9448.51,
    profile_pic: "http://dummyimage.com/227x100.png/5fa2dd/ffffff",
  },
  {
    id: 9,
    full_name: "Mel Martine",
    login_id: "mmartine8",
    salary: 8326.02,
    profile_pic: "http://dummyimage.com/240x100.png/cc0000/ffffff",
  },
  {
    id: 10,
    full_name: "Cathi Gellan",
    login_id: "cgellan9",
    salary: 8257.66,
    profile_pic: "http://dummyimage.com/179x100.png/5fa2dd/ffffff",
  },
  {
    id: 11,
    full_name: "Mariele Fusedale",
    login_id: "mfusedalea",
    salary: 8857.0,
    profile_pic: "http://dummyimage.com/227x100.png/dddddd/000000",
  },
  {
    id: 12,
    full_name: "Sayer Aronin",
    login_id: "saroninb",
    salary: 9401.94,
    profile_pic: "http://dummyimage.com/224x100.png/ff4444/ffffff",
  },
  {
    id: 13,
    full_name: "Kinnie Huc",
    login_id: "khucc",
    salary: 8033.44,
    profile_pic: "http://dummyimage.com/113x100.png/5fa2dd/ffffff",
  },
  {
    id: 14,
    full_name: "Rozalie Keble",
    login_id: "rkebled",
    salary: 8077.42,
    profile_pic: "http://dummyimage.com/192x100.png/ff4444/ffffff",
  },
  {
    id: 15,
    full_name: "Timmie Kilmartin",
    login_id: "tkilmartine",
    salary: 9868.3,
    profile_pic: "http://dummyimage.com/237x100.png/ff4444/ffffff",
  },
  {
    id: 16,
    full_name: "Elisabeth Milton",
    login_id: "emiltonf",
    salary: 9759.16,
    profile_pic: "http://dummyimage.com/135x100.png/cc0000/ffffff",
  },
  {
    id: 17,
    full_name: "Demetre Prine",
    login_id: "dprineg",
    salary: 8489.85,
    profile_pic: "http://dummyimage.com/104x100.png/5fa2dd/ffffff",
  },
  {
    id: 18,
    full_name: "Loleta Fahy",
    login_id: "lfahyh",
    salary: 9468.87,
    profile_pic: "http://dummyimage.com/180x100.png/cc0000/ffffff",
  },
  {
    id: 19,
    full_name: "Sherlock Inglesant",
    login_id: "singlesanti",
    salary: 9321.08,
    profile_pic: "http://dummyimage.com/133x100.png/dddddd/000000",
  },
  {
    id: 20,
    full_name: "Vivi Blinman",
    login_id: "vblinmanj",
    salary: 9193.38,
    profile_pic: "http://dummyimage.com/230x100.png/cc0000/ffffff",
  },
  {
    id: 21,
    full_name: "Alyssa Rann",
    login_id: "arannk",
    salary: 9351.89,
    profile_pic: "http://dummyimage.com/131x100.png/cc0000/ffffff",
  },
  {
    id: 22,
    full_name: "Olly Tivolier",
    login_id: "otivolierl",
    salary: 8307.83,
    profile_pic: "http://dummyimage.com/120x100.png/ff4444/ffffff",
  },
  {
    id: 23,
    full_name: "Darrelle Paszek",
    login_id: "dpaszekm",
    salary: 9308.67,
    profile_pic: "http://dummyimage.com/215x100.png/cc0000/ffffff",
  },
  {
    id: 24,
    full_name: "Hyacintha Edison",
    login_id: "hedisonn",
    salary: 8820.43,
    profile_pic: "http://dummyimage.com/192x100.png/dddddd/000000",
  },
  {
    id: 25,
    full_name: "Salvador Jasiak",
    login_id: "sjasiako",
    salary: 9898.48,
    profile_pic: "http://dummyimage.com/144x100.png/dddddd/000000",
  },
  {
    id: 26,
    full_name: "Danya Squibe",
    login_id: "dsquibep",
    salary: 9647.02,
    profile_pic: "http://dummyimage.com/187x100.png/dddddd/000000",
  },
  {
    id: 27,
    full_name: "Alberto Ales0",
    login_id: "aalesq",
    salary: 8731.54,
    profile_pic: "http://dummyimage.com/152x100.png/5fa2dd/ffffff",
  },
  {
    id: 28,
    full_name: "Alfy Heijne",
    login_id: "aheijner",
    salary: 9380.87,
    profile_pic: "http://dummyimage.com/119x100.png/dddddd/000000",
  },
  {
    id: 29,
    full_name: "Basil Coushe",
    login_id: "bcoushes",
    salary: 8069.56,
    profile_pic: "http://dummyimage.com/142x100.png/ff4444/ffffff",
  },
  {
    id: 30,
    full_name: "Cross Winsome",
    login_id: "cwinsomet",
    salary: 9716.89,
    profile_pic: "http://dummyimage.com/138x100.png/cc0000/ffffff",
  },
  {
    id: 31,
    full_name: "Powell Border",
    login_id: "pborderu",
    salary: 8234.62,
    profile_pic: "http://dummyimage.com/117x100.png/5fa2dd/ffffff",
  },
  {
    id: 32,
    full_name: "Sonnie Arnoult",
    login_id: "sarnoultv",
    salary: 9426.35,
    profile_pic: "http://dummyimage.com/158x100.png/ff4444/ffffff",
  },
  {
    id: 33,
    full_name: "Kennett Hallowell",
    login_id: "khallowellw",
    salary: 8963.31,
    profile_pic: "http://dummyimage.com/103x100.png/5fa2dd/ffffff",
  },
  {
    id: 34,
    full_name: "Elayne McChesney",
    login_id: "emcchesneyx",
    salary: 8576.92,
    profile_pic: "http://dummyimage.com/129x100.png/cc0000/ffffff",
  },
  {
    id: 35,
    full_name: "Tony Ker",
    login_id: "tkery",
    salary: 8452.44,
    profile_pic: "http://dummyimage.com/151x100.png/ff4444/ffffff",
  },
  {
    id: 36,
    full_name: "Judy McGougan",
    login_id: "jmcgouganz",
    salary: 9100.69,
    profile_pic: "http://dummyimage.com/118x100.png/dddddd/000000",
  },
  {
    id: 37,
    full_name: "Sayres Stirley",
    login_id: "sstirley10",
    salary: 9080.29,
    profile_pic: "http://dummyimage.com/152x100.png/5fa2dd/ffffff",
  },
  {
    id: 38,
    full_name: "Earvin Flarity",
    login_id: "eflarity11",
    salary: 9160.29,
    profile_pic: "http://dummyimage.com/118x100.png/ff4444/ffffff",
  },
  {
    id: 39,
    full_name: "Adelaide Rosenvasser",
    login_id: "arosenvasser12",
    salary: 9218.13,
    profile_pic: "http://dummyimage.com/189x100.png/ff4444/ffffff",
  },
  {
    id: 40,
    full_name: "Melly Prentice",
    login_id: "mprentice13",
    salary: 9580.14,
    profile_pic: "http://dummyimage.com/138x100.png/dddddd/000000",
  },
  {
    id: 41,
    full_name: "Lesya Subhan",
    login_id: "lsubhan14",
    salary: 8020.03,
    profile_pic: "http://dummyimage.com/120x100.png/cc0000/ffffff",
  },
  {
    id: 42,
    full_name: "Penny Currie",
    login_id: "pcurrie15",
    salary: 9608.26,
    profile_pic: "http://dummyimage.com/199x100.png/ff4444/ffffff",
  },
  {
    id: 43,
    full_name: "Graham Smethurst",
    login_id: "gsmethurst16",
    salary: 8008.74,
    profile_pic: "http://dummyimage.com/218x100.png/cc0000/ffffff",
  },
  {
    id: 44,
    full_name: "Trumaine Vivien",
    login_id: "tvivien17",
    salary: 9914.21,
    profile_pic: "http://dummyimage.com/243x100.png/ff4444/ffffff",
  },
  {
    id: 45,
    full_name: "Cully Darley",
    login_id: "cdarley18",
    salary: 8489.21,
    profile_pic: "http://dummyimage.com/226x100.png/ff4444/ffffff",
  },
  {
    id: 46,
    full_name: "Aleece Banane",
    login_id: "abanane19",
    salary: 9630.46,
    profile_pic: "http://dummyimage.com/176x100.png/ff4444/ffffff",
  },
  {
    id: 47,
    full_name: "Jarrod Couroy",
    login_id: "jcouroy1a",
    salary: 9919.88,
    profile_pic: "http://dummyimage.com/203x100.png/cc0000/ffffff",
  },
  {
    id: 48,
    full_name: "Shay Kilrow",
    login_id: "skilrow1b",
    salary: 9837.37,
    profile_pic: "http://dummyimage.com/153x100.png/5fa2dd/ffffff",
  },
  {
    id: 49,
    full_name: "Devondra Wotton",
    login_id: "dwotton1c",
    salary: 9582.33,
    profile_pic: "http://dummyimage.com/186x100.png/cc0000/ffffff",
  },
  {
    id: 50,
    full_name: "Solly Bettis",
    login_id: "sbettis1d",
    salary: 8888.15,
    profile_pic: "http://dummyimage.com/244x100.png/cc0000/ffffff",
  },
  {
    id: 51,
    full_name: "Ameline Angove",
    login_id: "aangove1e",
    salary: 8255.55,
    profile_pic: "http://dummyimage.com/102x100.png/ff4444/ffffff",
  },
  {
    id: 52,
    full_name: "Ruprecht Silkston",
    login_id: "rsilkston1f",
    salary: 8076.63,
    profile_pic: "http://dummyimage.com/170x100.png/cc0000/ffffff",
  },
  {
    id: 53,
    full_name: "Griffy Nilges",
    login_id: "gnilges1g",
    salary: 8732.49,
    profile_pic: "http://dummyimage.com/128x100.png/dddddd/000000",
  },
  {
    id: 54,
    full_name: "Margeaux Bloxham",
    login_id: "mbloxham1h",
    salary: 8275.1,
    profile_pic: "http://dummyimage.com/146x100.png/5fa2dd/ffffff",
  },
  {
    id: 55,
    full_name: "Jacquelyn Littledyke",
    login_id: "jlittledyke1i",
    salary: 8929.05,
    profile_pic: "http://dummyimage.com/182x100.png/cc0000/ffffff",
  },
  {
    id: 56,
    full_name: "Randie McKniely",
    login_id: "rmckniely1j",
    salary: 8651.97,
    profile_pic: "http://dummyimage.com/195x100.png/cc0000/ffffff",
  },
  {
    id: 57,
    full_name: "Wade Pladen",
    login_id: "wpladen1k",
    salary: 8600.02,
    profile_pic: "http://dummyimage.com/249x100.png/5fa2dd/ffffff",
  },
  {
    id: 58,
    full_name: "Jyoti Ellissen",
    login_id: "jellissen1l",
    salary: 9576.49,
    profile_pic: "http://dummyimage.com/126x100.png/5fa2dd/ffffff",
  },
  {
    id: 59,
    full_name: "Harris Kirsch",
    login_id: "hkirsch1m",
    salary: 8247.99,
    profile_pic: "http://dummyimage.com/130x100.png/cc0000/ffffff",
  },
  {
    id: 60,
    full_name: "Gay Cantrill",
    login_id: "gcantrill1n",
    salary: 8500.34,
    profile_pic: "http://dummyimage.com/118x100.png/cc0000/ffffff",
  },
  {
    id: 61,
    full_name: "Stace Wyvill",
    login_id: "swyvill1o",
    salary: 9522.19,
    profile_pic: "http://dummyimage.com/124x100.png/ff4444/ffffff",
  },
  {
    id: 62,
    full_name: "Carlotta Mulryan",
    login_id: "cmulryan1p",
    salary: 9926.2,
    profile_pic: "http://dummyimage.com/196x100.png/5fa2dd/ffffff",
  },
  {
    id: 63,
    full_name: "Olag Dickin",
    login_id: "odickin1q",
    salary: 8043.1,
    profile_pic: "http://dummyimage.com/203x100.png/5fa2dd/ffffff",
  },
  {
    id: 64,
    full_name: "Raina Stonary",
    login_id: "rstonary1r",
    salary: 9022.64,
    profile_pic: "http://dummyimage.com/118x100.png/dddddd/000000",
  },
  {
    id: 65,
    full_name: "Suellen Chadband",
    login_id: "schadband1s",
    salary: 8216.9,
    profile_pic: "http://dummyimage.com/177x100.png/ff4444/ffffff",
  },
  {
    id: 66,
    full_name: "Ruthe Biscomb",
    login_id: "rbiscomb1t",
    salary: 8085.82,
    profile_pic: "http://dummyimage.com/132x100.png/dddddd/000000",
  },
  {
    id: 67,
    full_name: "Janot Koppens",
    login_id: "jkoppens1u",
    salary: 9780.79,
    profile_pic: "http://dummyimage.com/175x100.png/5fa2dd/ffffff",
  },
  {
    id: 68,
    full_name: "Northrup Elen",
    login_id: "nelen1v",
    salary: 9942.42,
    profile_pic: "http://dummyimage.com/169x100.png/5fa2dd/ffffff",
  },
  {
    id: 69,
    full_name: "Shem Buddington",
    login_id: "sbuddington1w",
    salary: 8816.35,
    profile_pic: "http://dummyimage.com/108x100.png/5fa2dd/ffffff",
  },
  {
    id: 70,
    full_name: "Rani Benzie",
    login_id: "rbenzie1x",
    salary: 9175.07,
    profile_pic: "http://dummyimage.com/104x100.png/cc0000/ffffff",
  },
  {
    id: 71,
    full_name: "Krissie Kempshall",
    login_id: "kkempshall1y",
    salary: 8976.26,
    profile_pic: "http://dummyimage.com/184x100.png/dddddd/000000",
  },
  {
    id: 72,
    full_name: "Lanae Pimlett",
    login_id: "lpimlett1z",
    salary: 9442.56,
    profile_pic: "http://dummyimage.com/125x100.png/cc0000/ffffff",
  },
  {
    id: 73,
    full_name: "Kaitlin Hadwen",
    login_id: "khadwen20",
    salary: 8565.1,
    profile_pic: "http://dummyimage.com/246x100.png/5fa2dd/ffffff",
  },
  {
    id: 74,
    full_name: "Angeline Gatchell",
    login_id: "agatchell21",
    salary: 9718.24,
    profile_pic: "http://dummyimage.com/109x100.png/5fa2dd/ffffff",
  },
  {
    id: 75,
    full_name: "Nadine Tuckie",
    login_id: "ntuckie22",
    salary: 9292.97,
    profile_pic: "http://dummyimage.com/203x100.png/5fa2dd/ffffff",
  },
  {
    id: 76,
    full_name: "Cherry Burbridge",
    login_id: "cburbridge23",
    salary: 8342.78,
    profile_pic: "http://dummyimage.com/172x100.png/5fa2dd/ffffff",
  },
  {
    id: 77,
    full_name: "Arden Chew",
    login_id: "achew24",
    salary: 9229.75,
    profile_pic: "http://dummyimage.com/100x100.png/dddddd/000000",
  },
  {
    id: 78,
    full_name: "Ethelred Lindenblatt",
    login_id: "elindenblatt25",
    salary: 8051.41,
    profile_pic: "http://dummyimage.com/237x100.png/ff4444/ffffff",
  },
  {
    id: 79,
    full_name: "Lorettalorna Vinnick",
    login_id: "lvinnick26",
    salary: 8818.85,
    profile_pic: "http://dummyimage.com/153x100.png/dddddd/000000",
  },
  {
    id: 80,
    full_name: "Alicia Venners",
    login_id: "avenners27",
    salary: 9284.42,
    profile_pic: "http://dummyimage.com/239x100.png/ff4444/ffffff",
  },
  {
    id: 81,
    full_name: "Ileane Gerhardt",
    login_id: "igerhardt28",
    salary: 9275.49,
    profile_pic: "http://dummyimage.com/125x100.png/dddddd/000000",
  },
  {
    id: 82,
    full_name: "Humfrid Felderer",
    login_id: "hfelderer29",
    salary: 8180.34,
    profile_pic: "http://dummyimage.com/218x100.png/5fa2dd/ffffff",
  },
  {
    id: 83,
    full_name: "Mil Rainsdon",
    login_id: "mrainsdon2a",
    salary: 9731.66,
    profile_pic: "http://dummyimage.com/194x100.png/ff4444/ffffff",
  },
  {
    id: 84,
    full_name: "Anne Colombier",
    login_id: "acolombier2b",
    salary: 8241.55,
    profile_pic: "http://dummyimage.com/141x100.png/cc0000/ffffff",
  },
  {
    id: 85,
    full_name: "Liliane Bene",
    login_id: "lbene2c",
    salary: 8551.82,
    profile_pic: "http://dummyimage.com/116x100.png/5fa2dd/ffffff",
  },
  {
    id: 86,
    full_name: "Lorenzo Fazzioli",
    login_id: "lfazzioli2d",
    salary: 8997.17,
    profile_pic: "http://dummyimage.com/158x100.png/ff4444/ffffff",
  },
  {
    id: 87,
    full_name: "Courtney Baudinet",
    login_id: "cbaudinet2e",
    salary: 9208.36,
    profile_pic: "http://dummyimage.com/185x100.png/dddddd/000000",
  },
  {
    id: 88,
    full_name: "Robyn Purvey",
    login_id: "rpurvey2f",
    salary: 9767.45,
    profile_pic: "http://dummyimage.com/124x100.png/cc0000/ffffff",
  },
  {
    id: 89,
    full_name: "Asa Standish-Brooks",
    login_id: "astandishbrooks2g",
    salary: 9445.42,
    profile_pic: "http://dummyimage.com/186x100.png/ff4444/ffffff",
  },
  {
    id: 90,
    full_name: "Joseph Willimont",
    login_id: "jwillimont2h",
    salary: 9827.9,
    profile_pic: "http://dummyimage.com/250x100.png/cc0000/ffffff",
  },
  {
    id: 91,
    full_name: "Kathie Cawker",
    login_id: "kcawker2i",
    salary: 9737.89,
    profile_pic: "http://dummyimage.com/236x100.png/dddddd/000000",
  },
  {
    id: 92,
    full_name: "Goraud Bendtsen",
    login_id: "gbendtsen2j",
    salary: 8804.67,
    profile_pic: "http://dummyimage.com/206x100.png/ff4444/ffffff",
  },
  {
    id: 93,
    full_name: "Darsie Ingarfield",
    login_id: "dingarfield2k",
    salary: 9188.58,
    profile_pic: "http://dummyimage.com/160x100.png/ff4444/ffffff",
  },
  {
    id: 94,
    full_name: "Nefen Macieiczyk",
    login_id: "nmacieiczyk2l",
    salary: 9438.75,
    profile_pic: "http://dummyimage.com/107x100.png/cc0000/ffffff",
  },
  {
    id: 95,
    full_name: "Carma Lezemere",
    login_id: "clezemere2m",
    salary: 9527.44,
    profile_pic: "http://dummyimage.com/159x100.png/ff4444/ffffff",
  },
  {
    id: 96,
    full_name: "Betti Bruster",
    login_id: "bbruster2n",
    salary: 9381.51,
    profile_pic: "http://dummyimage.com/105x100.png/dddddd/000000",
  },
  {
    id: 97,
    full_name: "Wallache Parrot",
    login_id: "wparrot2o",
    salary: 8013.37,
    profile_pic: "http://dummyimage.com/136x100.png/dddddd/000000",
  },
  {
    id: 98,
    full_name: "Tammi Obray",
    login_id: "tobray2p",
    salary: 9134.8,
    profile_pic: "http://dummyimage.com/194x100.png/cc0000/ffffff",
  },
  {
    id: 99,
    full_name: "Consuela Hance",
    login_id: "chance2q",
    salary: 8531.89,
    profile_pic: "http://dummyimage.com/226x100.png/dddddd/000000",
  },
  {
    id: 100,
    full_name: "Ianthe Bispham",
    login_id: "ibispham2r",
    salary: 9815.87,
    profile_pic: "http://dummyimage.com/234x100.png/dddddd/000000",
  },
];

const formProps = {
  name: "file",
  action: "https://www.mocky.io/v2/5cc8019d300000980a055e76",
  headers: {
    authorization: "authorization-text",
  },
  onChange(info) {
    if (info.file.status !== "uploading") {
      console.log(info.file, info.fileList);
    }
    if (info.file.status === "done") {
      message.success(`${info.file.name} file uploaded successfully`);
    } else if (info.file.status === "error") {
      message.error(`${info.file.name} file upload failed.`);
    }
  },
  beforeUpload(file) {
    const isCSV = file.type === "text/cvs";
    if (!isCSV) {
      message.error("You can only upload CSV file!");
    }
    const isLt2M = file.size / 1024 / 1024 < 2;
    if (!isLt2M) {
      message.error("CSV file must smaller than 2MB!");
    }
    return isCSV && isLt2M;
  },
};
// table code start
const columns = [
  {
    title: "Profile Image",
    dataIndex: "profile_pic",
    key: "profile_pic",
    render: (profile_pic, record) => (
      <>
        <Avatar
          className="shape-avatar"
          shape="square"
          size={40}
          src={profile_pic}
        ></Avatar>
      </>
    ),
  },
  {
    title: "User Id",
    dataIndex: "id",
    key: "id",
    sortDirections: ["descend"],
    sorter: (a, b) => a.id - b.id,
  },
  {
    title: "Full Name",
    dataIndex: "full_name",
    key: "full_name",
    sortDirections: ["descend"],
    sorter: (a, b) => a.full_name - b.full_name,
  },

  {
    title: "Login Id",
    key: "login_id",
    dataIndex: "login_id",
    sortDirections: ["descend"],
    sorter: (a, b) => a.login_id - b.login_id,
  },
  {
    title: "Salary (USD)",
    key: "salary",
    dataIndex: "salary",
    sortDirections: ["descend"],
    sorter: (a, b) => a.salary - b.salary,
  },
  {
    title: "Actions",
    key: "id",
    dataIndex: "id",
    render: (text, record) => (
      <Space size="middle">
        <a>Edit</a>
        <Popconfirm
          title="Are you sure？"
          onConfirm={() => handleOnDelete(record)}
          icon={<QuestionCircleOutlined style={{ color: "red" }} />}
        >
          <a href="#">Delete</a>
        </Popconfirm>
      </Space>
    ),
  },
];

const handleOnDelete = async ({ id }) => {
  try {
    const response = await axios.delete(
      `https://nphc-hr.free.beeceptor.com/employees/${id}`
    );
    if (response.status === 204) {
      message.success("Deleted successfully");
      window.location.reload();
    }
  } catch (err) {
    message.error("Error while deleting");
  }
};

function Employees() {
  const [employees, setEmployees] = useState([]);
  const [editEmployee, setEditEmployee] = useState(null);
  const [showEditModal, setShowEditModal] = useState(false);
  const [showUploadModal, setShowUploadModal] = useState(false);

  useEffect(() => {
    const fetchEmployees = async () => {
      try {
        const response = await axios.get(
          "https://nphc-hr.free.beeceptor.com/employees"
        );
        if (response.status === 200) {
          setEmployees(response.data);
        }
      } catch (error) {
        setEmployees(DATA);
        message.error("Error while fetching employees");
        message.info("Showing dummy data loaded locally");
      }
    };
    fetchEmployees();
  }, []);

  const handleEditEmployee = async (values) => {
    const { full_name, login_id, salary } = values;
    try {
      const response = await axios.put(
        `https://nphc-hr.free.beeceptor.com/employees/${editEmployee.id}`,
        {
          full_name,
          login_id,
          salary,
        }
      );
      if (response.status === 204) {
        message.success("Updated successfully");
        window.location.reload();
      }
    } catch (error) {
      message.error("Error while updating");
    }
  };

  return (
    <>
      <div className="tabled">
        <Row gutter={[24, 0]}>
          <Col xs="24" xl={24}>
            <Card
              bordered={false}
              className="criclebox tablespace mb-24"
              title="Employees Table"
              extra={
                <>
                  <Button
                    type="primary"
                    className="ant-full-box"
                    icon={<ToTopOutlined />}
                    onClick={() => setShowUploadModal(true)}
                  >
                    Click to Upload
                  </Button>
                </>
              }
            >
              <div className="table-responsive">
                <Table
                  columns={columns}
                  dataSource={employees}
                  pagination={true}
                  onRow={(record, rowIndex) => {
                    return {
                      onClick: (event) => {
                        setEditEmployee(record);
                        setShowEditModal(true);
                      },
                    };
                  }}
                  className="ant-border-space"
                />
              </div>
            </Card>
          </Col>
        </Row>
      </div>
      {/* UPLOAD MODAL */}
      <Modal
        title="Upload Employee CSV"
        visible={showUploadModal}
        onOk={() => setShowUploadModal(false)}
        onCancel={() => setShowUploadModal(false)}
      >
        <div className="uploadfile pb-15 shadow-none">
          <Upload {...formProps}>
            <Button
              type="dashed"
              className="ant-full-box"
              icon={<ToTopOutlined />}
            >
              Click to Upload
            </Button>
          </Upload>
        </div>
      </Modal>
      {/* Edit MODAL */}
      <Modal
        title="Edit Employee"
        visible={showEditModal}
        onCancel={() => setShowEditModal(false)}
        footer={null}
      >
        <div className="uploadfile pb-15 shadow-none">
          <Title level={3}>Employee ID: {editEmployee?.id}</Title>
          <Form
            name="basic"
            labelCol={{ span: 8 }}
            wrapperCol={{ span: 16 }}
            initialValues={{
              full_name: editEmployee?.full_name,
              login_id: editEmployee?.login_id,
              salary: editEmployee?.salary,
            }}
            onFinish={(values) => {
              handleEditEmployee(values);
            }}
            autoComplete="off"
          >
            <Form.Item label="Name" name="full_name">
              <Input />
            </Form.Item>

            <Form.Item label="Login" name="login_id">
              <Input />
            </Form.Item>

            <Form.Item label="Salary" name="salary">
              <InputNumber />
            </Form.Item>

            <Form.Item wrapperCol={{ offset: 8, span: 16 }}>
              <Button type="primary" htmlType="submit">
                Save
              </Button>
            </Form.Item>
          </Form>
        </div>
      </Modal>
    </>
  );
}

export default Employees;
